using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Reflection;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// ���������� ��������� �������
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args">args</param>
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            //�������������� �������� ������ � ��. ���� �������� �� �� ���������� ������ ��� ������� � ������ ������ ��� �� ��������� ��� ������ ������ ������������ ���������.
            builder.Services.AddSingleton<EfContext, EfContext>();

            builder.Services.AddAutoMapper(typeof(UserMappingProfile));

            builder.Services.AddHttpContextAccessor();

            builder.Services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new OpenApiInfo 
                { 
                    Title = "EqvantaUsersCRUD",
                    Version = "v1.0.0.0",
                    Description = "�������� �������"
                });
                option.AddSecurityDefinition("basic", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "���������� ������� ����������� ����� � ������:",
                    Name = "�����������",
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "basic"
                });

                option.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type=ReferenceType.SecurityScheme,
                                Id="basic"
                            }
                        },
                        new string[]{}
                    }
                });

                string xml_path = Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml");
                option.IncludeXmlComments(xml_path);
                //� ������� ��������� ������ � AddSwaggerGen �� ������ ConfigureServices:
                option.SchemaFilter<EnumTypesSchemaFilter>(xml_path);
            });
            builder.Services.AddAuthentication("BasicAuthentication").AddScheme<AuthenticationSchemeOptions, BasicAuthHandler>("BasicAuthentication", null);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}