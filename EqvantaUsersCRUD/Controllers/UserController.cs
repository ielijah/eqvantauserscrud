using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace EqvantaUsersCRUD.Controllers
{
    /// <summary>
    /// ���������� �������� CRUD ��� User
    /// </summary>
    [ApiController]
    [Route("api")]
    public class UserController : ControllerBase
    {

        private readonly ILogger<UserController> _logger;
        private readonly EfContext _context; 
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger">��������� ��������������</param>
        /// <param name="context">�������� ��</param>
        /// <param name="mapper">����������</param>
        public UserController(ILogger<UserController> logger, EfContext context, IMapper mapper)
        {
            _logger = logger;
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// �������� ������������
        /// </summary>
        /// <remarks>
        /// �������� ������������ �� ������, ������, �����, ���� � ���� ��������. �������� ��� ���������������.
        /// 
        /// ������ �������:
        ///     
        ///        curl -X 'POST' \
        ///         '~/api/Create' \
        ///         -H 'accept: text/plain' \
        ///         -H 'Authorization: Basic salt' \
        ///         -H 'Content-Type: multipart/form-data' \
        ///         -F 'Login=MyAwesomeUser' \
        ///         -F 'Password=123qweasd' \
        ///         -F 'Name=Ivan' \
        ///         -F 'Gender=1' \
        ///         -F 'Birthday=13.09.2012' \
        ///         -F 'Admin=false'
        ///
        /// </remarks>
        /// <param name="user">������������</param>
        /// <returns>���������� ������������� ������ �� ��������� ������</returns>
        /// <response code="201">���������� ������������� ������ �� ��������� ������</response>
        /// <response code="400">��� ������� � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpPost, Authorize(Roles = "Administrator")]
        [Route("Create")]
        [ProducesResponseType(typeof(Uri), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(IDictionary<string, string[]>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Uri>> Create([FromForm] CreateUserRequest user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var new_user = _mapper.Map<User>(user);
            new_user.CreatedBy = User.Identity?.Name ?? "system";

            try
            {
                _context.Add(new_user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.InnerException?.Message??ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Created(new Uri($"{Request.PathBase}/api/Read/{new_user.Guid}", UriKind.Relative), null);
        }

        /// <summary>
        /// ��������� �������� ���������� � ������� ������������
        /// </summary>
        /// <remarks>
        /// ��������� �������� ���������� �� ���������������� ������������
        /// 
        /// ������ �������:
        ///
        ///     curl -X 'GET' \
        ///       '~/api/Read' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt'
        ///
        /// </remarks>
        /// <returns>�������� ���������� � ������������</returns>
        /// <response code="200">���������� �������� ���������� �� ���������������� ������������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpGet("Read"), Authorize(Roles = "User")]
        [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserResponse>> Read()
        {
            UserResponse response;

            try
            {
                string? username = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
                string? password = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.UserData)?.Value;
                if(username == null || password == null)
                    return NotFound();

                User? user = await _context.Users.FirstOrDefaultAsync(x => x.Login == username && x.Password == password);
                if (user == null)
                    return NotFound();

                response = _mapper.Map<UserResponse>(user);
                return Ok(response);
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// ��������� �������� ���������� � ������������ �� ��
        /// </summary>
        /// <remarks>
        /// �������� ������������ �� GUID.
        /// 
        /// ������ �������:
        ///
        ///     curl -X 'GET' \
        ///       '~/api/Read/6f12f698-3f83-429a-bd75-7a712e687640' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt'
        ///
        /// </remarks>
        /// <param name="id">���������� ������������� ������������</param>
        /// <returns>�������� ���������� � ������������</returns>
        /// <response code="200">���������� �������� ���������� � ������������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpGet("Read/{id:guid}"), Authorize(Roles = "Administrator,User")]
        [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserResponse>> Read(Guid id)
        {
            UserResponse response;

            try
            {
                User? user = await _context.Users.FirstOrDefaultAsync(x => x.Guid == id && x.RevokedOn == null);
                if (user == null)
                    return NotFound();

                response = _mapper.Map<UserResponse>(user);
                if (User.IsInRole("Administrator"))
                    return Ok(response);
                if (User.Identity?.Name == user.Login)//User ������� ��������� ������ ��� ����� ������
                    return Ok(response);
                return BadRequest();
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// ��������� �������� ���������� � ������������ �� ������
        /// </summary>
        /// <remarks>
        /// �������� ������������ �� ������. ������� �������� �� �����������. �������� ��� ���������������.
        /// 
        /// ������ �������:
        /// 
        ///     curl -X 'GET' \
        ///       '~/api/Read/admin' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt'
        ///
        /// </remarks>
        /// <param name="login">����� ������������</param>
        /// <returns>�������� ���������� � ������������</returns>
        /// <response code="200">���������� �������� ���������� � ������������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpGet("Read/{login}"), Authorize(Roles = "Administrator")]
        [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserResponse>> Read(string login)
        {
            UserResponse response;

            try
            {
                User? user = await _context.Users.FirstOrDefaultAsync(x => x.Login == login.ToLower());
                if (user == null)
                    return NotFound();

                response = _mapper.Map<UserResponse>(user);
                return Ok(response);
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// ������ ������ ���� ������������� ������ �������� ���������� � ���������
        /// </summary>
        /// <remarks>
        /// ������ ������ ���� ������������� ������ �������� ���������� � ���������. �������� ��� ���������������.
        /// 
        /// ������ �������:
        /// 
        ///     curl -X 'GET' \
        ///       '~/api/Read/36' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt'
        ///
        /// </remarks>
        /// <param name="age">������� (������ ���)</param>
        /// <returns>�������� ���������� � ������������</returns>
        /// <response code="200">������ ���� ������������� ������ ������������� ��������</response>
        /// <response code="206">�������� ����� �� ���������� ��������� �������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpGet("Read/{age:int:min(1):max(150)}"), Authorize(Roles = "Administrator")]
        [ProducesResponseType(typeof(UserResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<UserResponse>), StatusCodes.Status206PartialContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserResponse>> Read(int age)
        {
            List<UserResponse> response;

            try
            {
                List<User> users = await _context.Users.Where(x => x.Birthday < DateTime.Now.AddYears(-age)).Take(1000).ToListAsync();
                if (users == null)
                    return NotFound();

                response = _mapper.Map<List<UserResponse>>(users);
                if (response.Count == 1000)
                    return StatusCode(StatusCodes.Status206PartialContent, response);
                return Ok(response);
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// ������ ������ ���� �������� �������������
        /// </summary>
        /// <remarks>
        /// �������� ������ ���� �������� �������������. ������������� �� ���� ��������. �������� ��� ���������������.
        /// 
        /// ������ �������:
        /// 
        ///     curl -X 'GET' \
        ///       '~/api/Read/Active' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt'
        ///
        /// </remarks>
        /// <returns>������ ������ ���� �������� �������������</returns>
        /// <response code="200">������ ���� �������� �������������. ������������� �� ���� ��������</response>
        /// <response code="206">�������� ����� �� ���������� ��������� �������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="403">�� ���������� ���� �� ���������� ��������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpGet("Read/Active"), Authorize(Roles = "Administrator")]
        [ProducesResponseType(typeof(List<UserResponse>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(List<UserResponse>), StatusCodes.Status206PartialContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<UserResponse>> ReadActive()
        {
            List<UserResponse> response;

            try
            {
                List<User> users = await _context.Users.Where(x => x.RevokedOn == null).OrderBy(x => x.CreatedOn).Take(1000).ToListAsync();
                if (!users.Any())
                    return NotFound();

                response = _mapper.Map<List<UserResponse>>(users);
                if (response.Count == 1000)
                    return StatusCode(StatusCodes.Status206PartialContent, response);
                return Ok(response);
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// ��������� ������, ������, �����, ���� ��� ���� �������� ������������ �� ������
        /// </summary>
        /// <remarks>
        /// ��������� ������, ������, �����, ���� ��� ���� �������� ������������ �� ������. ����� ������ �������������, ���� ����� ������������, ���� �� �������.
        /// 
        /// ������ �������:
        ///       
        ///     curl -X 'PUT' \
        ///       '~/api/Update-1' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt' \
        ///       -H 'Content-Type: multipart/form-data' \
        ///       -F 'CurrentLogin=User' \
        ///       -F 'NewLogin=User1' \
        ///       -F 'Password=qwe123asd' \
        ///       -F 'Name=Egor' \
        ///       -F 'Gender=0' \
        ///       -F 'Birthday=04.12.1989'
        ///
        /// </remarks>
        /// <param name="user">������������</param>
        /// <returns>���������� ������������� ������ �� ����������� ������</returns>
        /// <response code="202">���������� ������������� ������ �� ����������� ������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpPut("Update-1"), Authorize(Roles = "Administrator,User")]
        [ProducesResponseType(typeof(Uri), StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Uri>> Update1([FromForm] UpdateUserRequest user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (User.Identity != null && User.Identity.Name != user.CurrentLogin.ToLower() && !User.IsInRole("Administrator"))
                return BadRequest("�������������� ������ ������������� �������� ������ ���������������.");

            User? db_user = _context.Users.FirstOrDefault(x => x.Login == user.CurrentLogin.ToLower());
            if (db_user == null)
                return NotFound();

            _mapper.Map<UpdateUserRequest, User>(user, db_user);
            db_user.ModifiedBy = User.Identity?.Name ?? "system";

            try
            {
                _context.Update(db_user);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Accepted(new Uri($"{Request.PathBase}/api/Read/{db_user.Guid}", UriKind.Relative), null);
        }

        /// <summary>
        /// �������������� ������������
        /// </summary>
        /// <remarks>
        /// �������������� ������������. ������� ����� (RevokedOn, RevokedBy). �������� �������.
        /// 
        /// ������ �������:
        ///       
        ///     curl -X 'PUT' \
        ///       '~/api/Update-2?login=User' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt' \
        ///
        /// </remarks>
        /// <param name="login">����� ������������</param>
        /// <returns>���������� ������������� ������ �� ����������� ������</returns>
        /// <response code="202">���������� ������������� ������ �� ����������� ������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpPut("Update-2"), Authorize(Roles = "Administrator")]
        [ProducesResponseType(typeof(Uri), StatusCodes.Status202Accepted)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Uri>> Update2(string login)
        {
            try
            {
                User? user = await _context.Users.FirstOrDefaultAsync(x => x.Login == login.ToLower());
                if (user == null)
                    return NotFound();
                if (user.RevokedOn == null)
                    return BadRequest("������������ ��� �������");

                user.ModifiedBy = User.Identity?.Name ?? "system";
                user.ModifiedOn = DateTime.Now;
                user.RevokedBy = null;
                user.RevokedOn = null;

                _context.Update(user);
                await _context.SaveChangesAsync();

                return Accepted(new Uri($"{Request.PathBase}/api/Read/{user.Guid}", UriKind.Relative), null);
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// �������� ������������
        /// </summary>
        /// <remarks>
        /// �������� ������������ �� ������ ������ ��� ������. ��� ������ �������� ���������� ����������� RevokedOn � RevokedBy. �������� �������.
        /// 
        /// ������ �������:
        ///       
        ///     curl -X 'DELETE' \
        ///       '~/api/Delete?login=User&soft=true' \
        ///       -H 'accept: text/plain' \
        ///       -H 'Authorization: Basic salt' \
        ///
        /// </remarks>
        /// <param name="login">����� ������������</param>
        /// <param name="soft">������� "�������" ��������</param>
        /// <returns>���������� ������������� ������ �� ����������� ������</returns>
        /// <response code="200">���������� ������������� ������ �� ����������� ������</response>
        /// <response code="400">������ � �������</response>
        /// <response code="401">������ � ������ ��� ������</response>
        /// <response code="404">������������ �� ������</response>
        /// <response code="500">�������������� ������ �������</response>
        [HttpDelete("Delete"), Authorize(Roles = "Administrator")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete(string login, bool soft = true)
        {
            try
            {
                User? user = await _context.Users.FirstOrDefaultAsync(x => x.Login == login.ToLower());
                if (user == null)
                    return NotFound();
                if (soft)
                {
                    if (user.RevokedOn != null)
                        return BadRequest("������������ ��� �������");

                    user.ModifiedBy = User.Identity?.Name ?? "system";
                    user.ModifiedOn = DateTime.Now;
                    user.RevokedBy = User.Identity?.Name ?? "system";
                    user.RevokedOn = DateTime.Now;

                    _context.Update(user);
                }
                else
                    _context.Remove(user);

                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (DbException ex)
            {
                return BadRequest(ex.InnerException?.Message ?? ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}