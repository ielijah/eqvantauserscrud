﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Text;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Класс аутентификации и авторизации
    /// </summary>
    public class BasicAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        #region Property  
        readonly EfContext _context;
        #endregion

        #region Constructor  
        /// <summary>
        /// Инициализирует новый экземпляр класса BasicAuthHandler/>.
        /// </summary>
        /// <param name="options">The monitor for the options instance.</param>
        /// <param name="logger">The <see cref="ILoggerFactory"/>.</param>
        /// <param name="encoder">The <see cref="System.Text.Encodings.Web.UrlEncoder"/>.</param>
        /// <param name="clock">The <see cref="ISystemClock"/>.</param>
        /// <param name="context">контекст для работы с БД</param>
        public BasicAuthHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            EfContext context)
            : base(options, logger, encoder, clock)
        {
            _context = context;
        }
        #endregion
        /// <summary>
        /// Обработка аутентификации пользователя
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ArgumentException">Ошибка при аутентификации</exception>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                var authHeader = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);

                if (!(authHeader != null && authHeader.Parameter != null && authHeader.Parameter.Split(':').Any()))
                    return AuthenticateResult.Fail("Не указаны данные для авторизации");

                var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader.Parameter)).Split(':');
                string username = credentials.First().ToLower();
                string password = credentials.Last();

                if (!_context.Users.Any(x=>x.Login == username && x.Password == password && x.RevokedOn == null))
                    return AuthenticateResult.Fail("Логин или пароль указаны не верно, либо учетная запись отключена от сервиса");

                var claims = new List<Claim>() {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.UserData, password),
                new Claim(ClaimTypes.Role, "User")
                };
                if (_context.Users.First(x => x.Login == username && x.Password == password).Admin)
                    claims.Add(new Claim(ClaimTypes.Role, "Administrator"));

                var identity = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);
                var ticket = new AuthenticationTicket(principal, Scheme.Name);

                return AuthenticateResult.Success(ticket);
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail($"Ошибка при аутентификации: {ex.Message}");
            }
        }
    }
}
