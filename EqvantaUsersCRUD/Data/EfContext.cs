﻿using Microsoft.EntityFrameworkCore;
namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Контекст работы с БД
    /// </summary>
    public class EfContext : DbContext
    {
        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<User> Users { get; set; }


        /// <summary>
        /// Удаляет и создаёт новый экземпляр БД при каждом запуске
        /// </summary>
        public EfContext()
        {
            Database.EnsureDeleted();
            Database.Migrate();
        }

    //    public EfContext(DbContextOptions<EfContext> options)
    //: base(options)
    //    {
    //    }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseSqlite("Filename=users.db");
        }

        /// <summary>
        /// Заполняет БД данными по умолчанию
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(InitDataFactory.Users);
        }
    }
}
