﻿using System.Data;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Сид БД стартовыми значениями
    /// </summary>
    public static class InitDataFactory
    {
        /// <summary>
        /// Сид таблицы пользователей стартовыми значениями
        /// </summary>
        public static IEnumerable<User> Users => new List<User>()
        {
            new User()
            {
                Guid = Guid.NewGuid(),
                Login = "admin",
                Password = "123qweasd",
                Name = "Администратор",
                Gender = Gender.Male,
                Birthday = new DateTime(1991, 4, 4, 7, 0, 0),
                Admin = true,
                CreatedOn = DateTime.Now,
                CreatedBy = "system"
            },
            new User()
            {
                Guid = Guid.NewGuid(),
                Login = "user",
                Password = "123qweasd",
                Name = "Пользователь",
                Gender = Gender.Male,
                Birthday = new DateTime(1991, 4, 5, 7, 0, 0),
                Admin = false,
                CreatedOn = DateTime.Now,
                CreatedBy = "admin"
            },
            new User()
            {
                Guid = Guid.NewGuid(),
                Login = "feuser",
                Password = "123qweasd",
                Name = "Пользовательница",
                Gender = Gender.Female,
                Birthday = new DateTime(1991, 4, 6, 7, 0, 0),
                Admin = false,
                CreatedOn = DateTime.Now,
                CreatedBy = "admin"
            }
        };
    }
}
