﻿using AutoMapper;
using System;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Мэпинг модели данных с моделями запроса и ответа
    /// </summary>
    public class UserMappingProfile : Profile //https://habr.com/ru/post/649645/
    {
        /// <summary>
        /// Мэпинг модели данных с моделями запроса и ответа
        /// </summary>
        public UserMappingProfile()
        {
            CreateMap<CreateUserRequest, User>()
                .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login.ToLower()));
            CreateMap<UpdateUserRequest, User>()
                .ForMember(dest => dest.Admin, opt => opt.Ignore())
                .ForMember(dest => dest.Birthday, opt => opt.Condition((src, dest, srcMember) => srcMember!=null))
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedOn, opt => opt.Ignore())
                .ForMember(dest => dest.Gender, opt => opt.Condition(src => src.Gender != null))
                .ForMember(dest => dest.Guid, opt => opt.Ignore())
                .ForMember(dest => dest.Login, opt => opt.MapFrom(src => string.IsNullOrWhiteSpace(src.NewLogin) ? src.CurrentLogin.ToLower() : src.NewLogin.ToLower())) //мапим текущий логин, если поле новый логин не заполнено
                .ForMember(dest => dest.ModifiedBy, opt => opt.Ignore())
                .ForMember(dest => dest.ModifiedOn, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Name, opt => opt.Condition((src, dest, srcMember) => !string.IsNullOrWhiteSpace(srcMember)))
                .ForMember(dest => dest.Password, opt => opt.Condition((src, dest, srcMember) => !string.IsNullOrWhiteSpace(srcMember)))
                .ForMember(dest => dest.RevokedBy, opt => opt.Ignore())
                .ForMember(dest => dest.RevokedOn, opt => opt.Ignore());
            CreateMap<User, UserResponse>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.RevokedBy == null && src.RevokedOn == null));
        }
    }
}
