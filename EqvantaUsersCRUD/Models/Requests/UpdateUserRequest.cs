﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Модель при передачи запросов на обновление или редактирование пользователей
    /// </summary>
    public class UpdateUserRequest
    {
        /// <summary>
        /// Текущий логин (запрещены все символы кроме латинских букв и цифр)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Текущий логин пользователя обязателен.")]
        [DisplayName("Текущий логин пользователя")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Логина 30 символов.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Запрещены все символы кроме латинских букв и цифр.")]
        [LoginValidation(ErrorMessage = "Текущий Логин содержит зарезервированное системное слово.")]
        public string CurrentLogin { get; set; } = null!;

        /// <summary>
        /// Новый логин (запрещены все символы кроме латинских букв и цифр)
        /// </summary>
        [DisplayName("Новый Логин (опционально)")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Логина 30 символов.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Запрещены все символы кроме латинских букв и цифр.")]
        [LoginValidation(ErrorMessage = "Новый Логин содержит зарезервированное системное слово.")]
        public string? NewLogin { get; set; } = null!;

        /// <summary>
        /// Пароль (запрещены все символы кроме латинских букв и цифр)
        /// </summary>
        [DisplayName("Новый Пароль (опционально)")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Пароля 30 символов.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Запрещены все символы кроме латинских букв и цифр.")]
        public string? Password { get; set; } = null!;

        /// <summary>
        /// Имя (запрещены все символы кроме латинских и русских букв)
        /// </summary>
        [DisplayName("Новое имя (опционально)")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Имени 30 символов.")]
        [RegularExpression(@"^[a-zA-Zа-яА-Я]+$", ErrorMessage = "Запрещены все символы кроме латинских и русских букв.")]
        public string? Name { get; set; } = null!;

        /// <summary>
        /// Пол
        /// </summary>
        [DisplayName("Новый пол (опционально)")]
        public Gender? Gender { get; set; }

        /// <summary>
        /// Дата рождения может быть Null
        /// </summary>
        [DisplayName("Новая дата рождения (опционально)")]
        public DateTime? Birthday { get; set; }

        ///// <summary>
        ///// Указание - является ли пользователь админом
        ///// </summary>
        //[DefaultValue(false)]
        //[DisplayName("Администратор (опционально)")]
        //public bool Admin { get; set; }

    }
}
