﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Модель при передачи запросов на обновление или редактирование пользователей
    /// </summary>
    public class CreateUserRequest
    {
        /// <summary>
        /// Уникальный Логин (запрещены все символы кроме латинских букв и цифр)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Логин обязателен.")]
        [DisplayName("Логин")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Логина 30 символов.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Запрещены все символы кроме латинских букв и цифр.")]
        [LoginValidation(ErrorMessage = "Логин содержит зарезервированное системное слово.")]
        public string Login { get; set; } = null!;

        /// <summary>
        /// Пароль (запрещены все символы кроме латинских букв и цифр)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Пароль обязателен.")]
        [DisplayName("Пароль")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Пароля 30 символов.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Запрещены все символы кроме латинских букв и цифр.")]
        public string Password { get; set; } = null!;

        /// <summary>
        /// Имя (запрещены все символы кроме латинских и русских букв)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Имя обязательно.")]
        [DisplayName("Имя")]
        [MaxLength(30, ErrorMessage = "Максимальная длина Имени 30 символов.")]
        [RegularExpression(@"^[a-zA-Zа-яА-Я]+$", ErrorMessage = "Запрещены все символы кроме латинских и русских букв.")]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Пол
        /// </summary>
        [Required]
        [DefaultValue(Gender.Female)]
        [DisplayName("Пол")]
        public Gender Gender { get; set; }

        /// <summary>
        /// Дата рождения может быть Null
        /// </summary>
        [DisplayName("Дата рождения")]
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// Указание - является ли пользователь админом
        /// </summary>
        [DefaultValue(false)]
        [DisplayName("Администратор")]
        public bool Admin { get; set; }

    }
}
