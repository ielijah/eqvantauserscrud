using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Index = Microsoft.EntityFrameworkCore.Metadata.Internal.Index;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// ���
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// �������
        /// </summary>
        Female,
        /// <summary>
        /// �������
        /// </summary>
        Male,
        /// <summary>
        /// ����������
        /// </summary>
        Unknown
    };

    /// <summary>
    /// ������ ������ ������������
    /// </summary>
    [Index("Guid", IsUnique = true, Name = "Guid_Index")]
    [Index("Login", IsUnique = true, Name = "Login_Index")]
    public class User
    {
        /// <summary>
        /// ���������� ������������� ������������
        /// </summary>
        [Key]
        [Required]
        [DisplayName("��")]
        public Guid Guid { get; set; }

        /// <summary>
        /// ���������� ����� (��������� ��� ������� ����� ��������� ���� � ����)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "����� ����������.")]
        [DisplayName("�����")]
        [MaxLength(30, ErrorMessage = "������������ ����� ������ 30 ��������.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "��������� ��� ������� ����� ��������� ���� � ����.")]
        public string Login { get; set; } = null!;

        /// <summary>
        /// ������ (��������� ��� ������� ����� ��������� ���� � ����)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "������ ����������.")]
        [DisplayName("������")]
        [MaxLength(30, ErrorMessage = "������������ ����� ������ 30 ��������.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "��������� ��� ������� ����� ��������� ���� � ����.")]
        public string Password { get; set; } = null!;

        /// <summary>
        /// ��� (��������� ��� ������� ����� ��������� � ������� ����)
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "��� �����������.")]
        [DisplayName("���")]
        [MaxLength(30, ErrorMessage = "������������ ����� ����� 30 ��������.")]
        [RegularExpression(@"^[a-zA-Z�-��-�]+$", ErrorMessage = "��������� ��� ������� ����� ��������� � ������� ����.")]
        public string Name { get; set; } = null!;

        /// <summary>
        /// ���
        /// </summary>
        [Required]
        [DisplayName("���")]
        [DefaultValue(Gender.Male)]
        public Gender Gender { get; set; }

        /// <summary>
        /// ���� �������� ����� ���� Null
        /// </summary>
        [DisplayName("���� ��������")]
        public DateTime? Birthday { get; set; }

        /// <summary>
        /// �������� - �������� �� ������������ �������
        /// </summary>
        [Required]
        [DisplayName("�������������")]
        [DefaultValue(false)]
        public bool Admin { get; set; }

        /// <summary>
        /// ���� �������� ������������
        /// </summary>
        [Required]
        [DisplayName("���� ��������")]
        public DateTime CreatedOn { get; set; } = DateTime.Now;

        /// <summary>
        /// ����� ������������, �� ����� �������� ���� ������������ ������
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "������� ��� ������������ ���������� ������������.")]
        [DisplayName("������")]
        public string CreatedBy { get; set; } = null!;

        /// <summary>
        /// ���� ��������� ������������
        /// </summary>
        [DisplayName("���� ���������")]
        public DateTime? ModifiedOn { get; set; }

        /// <summary>
        /// ����� ������������, �� ����� �������� ���� ������������ �������
        /// </summary>
        [DisplayName("�������")]
        [MaxLength(30, ErrorMessage = "������������ ����� ������ 30 ��������.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "��������� ��� ������� ����� ��������� ���� � ����.")]
        public string? ModifiedBy { get; set; }

        /// <summary>
        /// ���� �������� ������������
        /// </summary>
        [DisplayName("���� ��������")]
        public DateTime? RevokedOn { get; set; }

        /// <summary>
        /// ����� ������������, �� ����� �������� ���� ������������ �����
        /// </summary>
        [DisplayName("������")]
        [MaxLength(30, ErrorMessage = "������������ ����� ������ 30 ��������.")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "��������� ��� ������� ����� ��������� ���� � ����.")]
        public string? RevokedBy { get; set; }
    }
}