﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Проверка поля Логин на содержание зарезервированных слов
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    sealed public class LoginValidationAttribute : ValidationAttribute
    {
        /// <summary>
        /// Проверка поля Логин на содержание зарезервированных слов
        /// </summary>
        /// <param name="value">Значение поля Логин для проверки</param>
        /// <returns>true - поле не содержит зарезервированных слов</returns>
        public override bool IsValid(object? value)
        {
            if(value == null)
                return true;
            string result = ((string)value).ToLower();
            return result != "active" && result != "inactive" && result != "system";
        }
    }
}
