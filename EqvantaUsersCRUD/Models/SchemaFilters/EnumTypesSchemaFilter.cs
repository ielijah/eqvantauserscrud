﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Xml.Linq;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// Расширение для удобного представления данных в Swagger
    /// <see href="https://habr.com/ru/company/simbirsoft/blog/707108/"/>
    /// </summary>
    public class EnumTypesSchemaFilter : ISchemaFilter
    {
        private readonly XDocument _xmlComments = null!;
        /// <summary>
        /// Инициализирует экземпляр класса для отображения перечислений в Swagger
        /// </summary>
        /// <param name="xmlPath">Путь к файлу с документацией</param>
        public EnumTypesSchemaFilter(string xmlPath)
        {
            if (File.Exists(xmlPath))
            {
                _xmlComments = XDocument.Load(xmlPath);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            if (_xmlComments == null) return;

            if (schema.Enum != null && schema.Enum.Count > 0 &&
                context.Type != null && context.Type.IsEnum)
            {
                schema.Description += "<p>Содержит значения:</p><ul>";

                var fullTypeName = context.Type.FullName;

                foreach (var enumMemberName in Enum.GetValues(context.Type))
                {
                    var enumMemberValue = Convert.ToInt64(enumMemberName);

                    var fullEnumMemberName = $"F:{fullTypeName}.{enumMemberName}";

                    var enumMemberComments = _xmlComments.Descendants("member")
                        .FirstOrDefault(m => m.Attribute("name")!.Value.Equals
                        (fullEnumMemberName, StringComparison.OrdinalIgnoreCase));

                    if (enumMemberComments == null) continue;

                    var summary = enumMemberComments.Descendants("summary").FirstOrDefault();

                    if (summary == null) continue;

                    schema.Description += $"<li><i>{enumMemberValue}</i> - {summary.Value.Trim()}</li>";
                }

                schema.Description += "</ul>";
            }
        }
    }
}
