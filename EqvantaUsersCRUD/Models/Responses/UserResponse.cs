using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Index = Microsoft.EntityFrameworkCore.Metadata.Internal.Index;

namespace EqvantaUsersCRUD
{
    /// <summary>
    /// ������ ��� �������� ������ � ������� � ������������
    /// </summary>
    public class UserResponse
    {
        ///// <summary>
        ///// ���������� ������������� ������������
        ///// </summary>
        //[DisplayName("��")]
        //public Guid Guid { get; set; }

        ///// <summary>
        ///// ���������� �����
        ///// </summary>
        //[DisplayName("�����")]
        //public string Login { get; set; } = string.Empty;

        ///// <summary>
        ///// ������
        ///// </summary>
        //[DisplayName("������")]
        //public string Password { get; set; } = string.Empty;

        /// <summary>
        /// ���
        /// </summary>
        [DisplayName("���")]
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// ���
        /// </summary>
        [DisplayName("���")]
        public Gender Gender { get; set; }

        /// <summary>
        /// ���� ��������
        /// </summary>
        [DisplayName("���� ��������")]
        public DateTime? Birthday { get; set; }

        ///// <summary>
        ///// �������� - �������� �� ������������ �������
        ///// </summary>
        //[DisplayName("�������������")]
        //public bool Admin { get; set; }

        ///// <summary>
        ///// ���� �������� ������������
        ///// </summary>
        //[DisplayName("���� ��������")]
        //public DateTime CreatedOn { get; set; }

        /// <summary>
        /// �������� - �������� �� ������������ �������� (����������� RevokedOn)
        /// </summary>
        [DisplayName("��������")]
        public bool IsActive { get; set; }
    }
}