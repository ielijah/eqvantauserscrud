﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EqvantaUsersCRUD.Migrations
{
    /// <inheritdoc />
    public partial class _4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("92514803-80c1-4984-86f7-54f835ae868c"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("93aab5ef-3ec6-4e08-a07a-1e22135f49bf"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("9f1b5a5a-0718-443e-92d4-73e37fc4e9c9"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Guid", "Admin", "Birthday", "CreatedBy", "CreatedOn", "Gender", "Login", "ModifiedBy", "ModifiedOn", "Name", "Password", "RevokedBy", "RevokedOn" },
                values: new object[,]
                {
                    { new Guid("173942bd-aaa8-4007-b66b-bfcbe74adbc0"), false, new DateTime(1991, 4, 5, 7, 0, 0, 0, DateTimeKind.Unspecified), "admin", new DateTime(2023, 1, 7, 14, 43, 40, 541, DateTimeKind.Local).AddTicks(8035), 1, "user", null, null, "Пользователь", "123qweasd", null, null },
                    { new Guid("6f12f698-3f83-429a-bd75-7a712e687640"), false, new DateTime(1991, 4, 6, 7, 0, 0, 0, DateTimeKind.Unspecified), "admin", new DateTime(2023, 1, 7, 14, 43, 40, 541, DateTimeKind.Local).AddTicks(8038), 0, "feuser", null, null, "Пользовательница", "123qweasd", null, null },
                    { new Guid("cc6ef58b-9891-4a1b-932a-b910e3adf21e"), true, new DateTime(1991, 4, 4, 7, 0, 0, 0, DateTimeKind.Unspecified), "system", new DateTime(2023, 1, 7, 14, 43, 40, 541, DateTimeKind.Local).AddTicks(8031), 1, "admin", null, null, "Администратор", "123qweasd", null, null }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("173942bd-aaa8-4007-b66b-bfcbe74adbc0"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("6f12f698-3f83-429a-bd75-7a712e687640"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("cc6ef58b-9891-4a1b-932a-b910e3adf21e"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Guid", "Admin", "Birthday", "CreatedBy", "CreatedOn", "Gender", "Login", "ModifiedBy", "ModifiedOn", "Name", "Password", "RevokedBy", "RevokedOn" },
                values: new object[,]
                {
                    { new Guid("92514803-80c1-4984-86f7-54f835ae868c"), false, new DateTime(1991, 4, 5, 7, 0, 0, 0, DateTimeKind.Unspecified), "Admin", new DateTime(2023, 1, 7, 14, 43, 5, 632, DateTimeKind.Local).AddTicks(6395), 1, "user", null, null, "Пользователь", "123qweasd", null, null },
                    { new Guid("93aab5ef-3ec6-4e08-a07a-1e22135f49bf"), true, new DateTime(1991, 4, 4, 7, 0, 0, 0, DateTimeKind.Unspecified), "system", new DateTime(2023, 1, 7, 14, 43, 5, 632, DateTimeKind.Local).AddTicks(6389), 1, "admin", null, null, "Администратор", "123qweasd", null, null },
                    { new Guid("9f1b5a5a-0718-443e-92d4-73e37fc4e9c9"), false, new DateTime(1991, 4, 6, 7, 0, 0, 0, DateTimeKind.Unspecified), "Admin", new DateTime(2023, 1, 7, 14, 43, 5, 632, DateTimeKind.Local).AddTicks(6398), 0, "feuser", null, null, "Пользовательница", "123qweasd", null, null }
                });
        }
    }
}
