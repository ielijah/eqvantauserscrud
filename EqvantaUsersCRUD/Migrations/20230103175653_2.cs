﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EqvantaUsersCRUD.Migrations
{
    /// <inheritdoc />
    public partial class _2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("6173c555-82c8-4d11-990b-53546c70b265"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Guid", "Admin", "Birthday", "CreatedBy", "CreatedOn", "Gender", "Login", "ModifiedBy", "ModifiedOn", "Name", "Password", "RevokedBy", "RevokedOn" },
                values: new object[,]
                {
                    { new Guid("2aecba79-d11a-457e-af42-586d877f61ff"), false, new DateTime(1991, 4, 6, 7, 0, 0, 0, DateTimeKind.Unspecified), "Admin", new DateTime(2023, 1, 3, 20, 56, 53, 284, DateTimeKind.Local).AddTicks(7221), 0, "FeUser", null, null, "Пользовательница", "123qweasd", null, null },
                    { new Guid("3945305d-a217-4f1f-a60e-16c980f58f3a"), true, new DateTime(1991, 4, 4, 7, 0, 0, 0, DateTimeKind.Unspecified), "system", new DateTime(2023, 1, 3, 20, 56, 53, 284, DateTimeKind.Local).AddTicks(7199), 1, "Admin", null, null, "Администратор", "123qweasd", null, null },
                    { new Guid("d4d9d19f-9997-47bd-aac4-1b616997ba9d"), false, new DateTime(1991, 4, 5, 7, 0, 0, 0, DateTimeKind.Unspecified), "Admin", new DateTime(2023, 1, 3, 20, 56, 53, 284, DateTimeKind.Local).AddTicks(7216), 1, "User", null, null, "Пользователь", "123qweasd", null, null }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("2aecba79-d11a-457e-af42-586d877f61ff"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("3945305d-a217-4f1f-a60e-16c980f58f3a"));

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Guid",
                keyValue: new Guid("d4d9d19f-9997-47bd-aac4-1b616997ba9d"));

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Guid", "Admin", "Birthday", "CreatedBy", "CreatedOn", "Gender", "Login", "ModifiedBy", "ModifiedOn", "Name", "Password", "RevokedBy", "RevokedOn" },
                values: new object[] { new Guid("6173c555-82c8-4d11-990b-53546c70b265"), true, new DateTime(1991, 4, 4, 7, 0, 0, 0, DateTimeKind.Unspecified), "system", new DateTime(2022, 12, 29, 23, 0, 53, 542, DateTimeKind.Local).AddTicks(2689), 1, "Admin", null, null, "Администратор", "123qweasd", null, null });
        }
    }
}
